Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get '/' => 'fms#index', as: :fms_index
  get '/:artist' => 'fms#show', as: :fms_detail

end

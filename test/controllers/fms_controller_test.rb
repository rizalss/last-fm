require "test_helper"

class FmsControllerTest < ActionDispatch::IntegrationTest
  # setup do
  #   @fm = fms(:one)
  # end

  test "should get index" do
    get fms_index_url
    assert_response :success
  end

  test "should get ajax request data top artist first page" do
    get fms_index_url, params: { dataType: 'json', offset: 0, limit: 5, search: 'indonesia' }
    assert_response :success
  end

  test "should get data top track" do
    get fms_detail_url('coldplay')
    assert_response :success
  end

  # test "should get new" do
  #   get new_fm_url
  #   assert_response :success
  # end
  #
  # test "should create fm" do
  #   assert_difference('Fm.count') do
  #     post fms_url, params: { fm: {  } }
  #   end
  #
  #   assert_redirected_to fm_url(Fm.last)
  # end
  #
  # test "should show fm" do
  #   get fm_url(@fm)
  #   assert_response :success
  # end
  #
  # test "should get edit" do
  #   get edit_fm_url(@fm)
  #   assert_response :success
  # end
  #
  # test "should update fm" do
  #   patch fm_url(@fm), params: { fm: {  } }
  #   assert_redirected_to fm_url(@fm)
  # end
  #
  # test "should destroy fm" do
  #   assert_difference('Fm.count', -1) do
  #     delete fm_url(@fm)
  #   end
  #
  #   assert_redirected_to fms_url
  # end
end

require "application_system_test_case"
require "capybara/rails"

class FmsTest < ApplicationSystemTestCase
  test "visiting the index" do
    visit fms_index_url
    assert_selector 'table#table-artist'
    assert_selector 'table#table-artist th div.th-inner', "Most Popular Artist"
  end
end

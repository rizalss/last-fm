class FmsController < ApplicationController
  include ApplicationHelper

  # list popular artist by country
  def index
    # json request
    if params[:dataType] == 'json'
      # page param for paging
      page = (params[:offset].to_i/params[:limit].to_i) + 1
      # get data rows from api last.fm
      rows = JSON.parse(get_by_country(params[:search], params[:limit], page.to_s).get)

      # data found by country
      if !rows['error'].present?
        # init total data count
        total = JSON.parse(get_by_country(params[:search], nil, nil).get)['topartists']['artist'].length
        # init data rows
        @data = rows['topartists']['artist']

        # add link each row data
        @data.each_with_index do |x, i|
          x['link'] = x['name'].downcase.gsub(' ', '-')
        end
        render json: { rows: @data, total: total }
      else
        render json: { rows: [], total: 0 }
      end
    else
      # render template
      render :index
    end
  end

  # top track list by artist
  def show
    # re-format str url link
    artist = params[:artist].gsub('-', ' ')
    # get data rows from api last.fm
    @api = JSON.parse(get_top_track(artist, params[:limit], params[:page]).get)['toptracks']['track']
    # render template
    render :detail
  end
end

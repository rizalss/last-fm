module ApplicationHelper
  # get top track by country
  def get_by_country(country, limit, page)
    # init params
    country = country.nil? ? '' : country
    q_page = !page.nil? ? '&page='+page : ''
    q_limit = !limit.nil? ? '&limit='+limit : ''

    # query url with parms
    query_url ="http://ws.audioscrobbler.com/2.0/?format=json#{q_page}#{q_limit}&api_key=ec628800665b879ab366c5c362e64561&method=geo.gettopartists&country=#{country}"

    # get data from api using query url
    RestClient::Resource.new(
      query_url,
      :verify_ssl =>  false
    )
  end

  # get top track by artist
  def get_top_track(artist, limit, pag)
    # query url with parms
    query_url = "http://ws.audioscrobbler.com/2.0/?format=json&page=1&api_key=ec628800665b879ab366c5c362e64561&method=artist.gettoptracks&artist=#{artist}"

    # get data from api using query url
    RestClient::Resource.new(
      query_url,
      :verify_ssl =>  false
    )
  end
end
